"""
CentralLimitThm_starter.py

This script gets students started on the CentralLimitTheorem problem. Shows them how to pull random values from
different distributions and plot histograms.

Origin: CHE 528 Problem development.
Author: Tom Allison
Started: 1/29/2022
"""

#%%
# Preliminaries ===============================================================
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.stats import expon
from scipy.stats import uniform

#%%
# Define Parameters ===========================================================
n = 2**16 # number of samples to pull from the distribution
Nbins = 2**8 # number of bins to use for constructing histograms
L = 1
x1_n = np.zeros( (n,) )  # initialize empty array to store random molecule positions
bin_edges = np.linspace(0,L,Nbins+1) # define bin edges for histogram
x =(1/L)*(bin_edges[0:-1] + bin_edges[1:]) / 2 # bin centers. x is really x/L, but it is the same if L = 1
dx = x[1]-x[0]

#%%
# Generate random samples and histogram =======================================
for k in np.arange(n):
    x1_n[k] = uniform.rvs()            # pull random molecule positions from uniform distribution
    #x1_n[k] = expon.rvs(scale = 0.1)  # pull random molecule positions from exp(-10x/L) distribution

X, be = np.histogram(x1_n, bins = bin_edges) # histrogram the results
dPdx = X/(n*dx)   # convert to probability density
Ptot = dx*np.trapz(dPdx) # check normalization

#%%
# Plot results =================================================================
fig = plt.figure()
ax = plt.axes()
ax.plot(x,dPdx)
ax.set_ylabel('Probability density')
ax.set_xlabel('<x>(N)')
ax.set_ylim(0,1.2*np.max(dPdx)) # scale y axis

plt.show()


