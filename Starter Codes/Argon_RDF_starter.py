"""
Argon_RDF_starter.py

This script loads and plots S(k) data for liquid argon from Yarnell_PRA1973.

Origin: CHE 528/PHY 540 Problem devleopment
Author: Tom Allison
"""

#%% 
# Preliminaries ===============================================================

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.integrate import cumtrapz

#%%
# Load S(k) and plot ==========================================================

DF = pd.read_csv('Yarnell_PRA1973_QSQonly_onecolumn.csv')
k = DF['Q']
Sk = DF['S(Q)']

fig = plt.figure()
ax = plt.axes()
ax.plot(k,Sk, label = '$S(k)$', linewidth = 3)
ax.grid()
ax.legend()
ax.set_xlabel('$k \;\; [\AA^{-1}]$')
ax.set_ylabel('$S(k)$')

plt.show()


