"""
DiracMaterials_starter.py

Starter code for DiracMaterials problem. Illustrates how to solve an equation involving an integral numerically.

Origin: PHY 540 Problem Development
Author: Tom Allison
"""

#%%
# Preliminaries =================================================================

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from scipy.optimize import fsolve
from scipy.integrate import simpson

#%%
# Define global variables =====================================================
x = np.linspace(-25,25,2**16)
dx = x[1]-x[0]

#%%
# define function =============================================================

def ZeroMe(z,y):
    """ function to find roots of"""
    return y - z**2*simpson(x*(1/np.cosh(x-z))**2)*dx

#%%
# Solve equation for various y ================================================

y = np.linspace(-10,10) # set up array of y values to step through.
zsol = np.zeros( (len(y),) ) #array to store solutions.

for j in range(len(y)):
    zguess = y[j]/10 # initial guess along a line intersecting the orign.
    zsol[j] = fsolve(ZeroMe,zguess,y[j],xtol=1.49012e-09) #find root of ZeroMe, store result in zsol.

#%%
# Plot results ================================================================

fig = plt.figure()
ax = plt.axes()
ax.plot(y,zsol, marker = 'o')
ax.grid()
ax.set_xlabel('y')
ax.set_ylabel('z')
ax.set_title('Solution to $y - z^2\int dx \; x \sech^2(x-z) = 0$')

plt.show()

    
    



