"""
plotBBR.py

This script plots the Blackbody radiation radiation spectrum for various
realistic scenarios.

Origin: CHE 528 Lecture Development
Author: Tom Allison
"""
#%%
# Preliminaries =================================================================
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams.update({'font.size': 14})
matplotlib.rcParams.update({'figure.figsize': (10,12)})

#%%
# Define constants =============================================================
hbar = 1.055E-34 # Planck's constant in SI
c = 2.9972E8     # Speed of light in SI
kB = 1.38E-23    # Boltzmann's constant in SI
w = np.linspace(1E12,3E16,5000) # angular frequency in Hz
lam = 2*np.pi*c/w               # wavelength in meters

T = [300,1500,5800]
uw = np.zeros( (len(T),len(w)) )
ulam = np.zeros( (len(T),len(w)) )
legstring = []

for j in range(len(T)):
    bhw = hbar*w/(kB*T[j])
    uw[j,:] = (hbar * w**3)/(np.pi**2 * c**3)* 1/(np.exp(bhw) -1)
    ulam[j,:] = 16* (np.pi**2 * c * hbar)/( lam**5 ) * 1/(np.exp(bhw) -1)
    legstring.append(f'T = {T[j]} K')
    
nuTHz =  w*(1E-12/(2*np.pi))   #nu in THz
dudnu = uw*(2*np.pi)*1E12      #Energy density per THz of BW

lam_nm = lam*1E9               #wavelength in nanometers
dudlam_nm = ulam*1E-9          #Energy density per nanonmeter of BW


#%%
# Plot results ================================================================

fig, (ax1,ax2) = plt.subplots(2,1, sharex= False)
ax1.plot(nuTHz.T, dudnu.T)
ax1.grid()
ax1.set_xlabel('$\\nu$ [THz]')
ax1.set_ylabel('$du/d\\nu$ [J/m$^3$-THz]')
ax1.legend(legstring)

ax2.loglog(lam_nm.T, dudlam_nm.T)
ax2.set_xlim([40000,100])
ax2.set_ylim([1E-8*np.max(dudlam_nm),2*np.max(dudlam_nm)])
ax2.grid()
ax2.set_xlabel('$\lambda [nm]$')
ax2.set_ylabel('$du/d\lambda$ [J/m$^3$-nm]')
ax2.legend(legstring)

plt.show()



