"""
BECplots.py

This script calculates various parameters related to Bose-Einstein condensation
as a function of T or N.

Origin: PHY 540 Lecture development.
Author: Tom Allison
"""

#%% 
# Preliminaries ===============================================================
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import gamma
from scipy.integrate import quad
from scipy.optimize import brentq
import matplotlib

matplotlib.rcParams.update({'font.size': 13})


#%%
# Define functions ============================================================

def integrand(x,z,nu):
    """ Integrand of Bose-Einstein Function"""
    return x**(nu-1)*np.exp(-x)/ ( (1/z) - np.exp(-x) ) # phrase in terms of e^(-x) to avoid overflow.
    

def g_nu1(z,nu):
    """ Bose-Einstein function"""
    return (1/gamma(nu))*quad(integrand, 0, np.inf, args = (z,nu))[0]

g_nu = np.vectorize(g_nu1)

def ZeroMe(z,T):
    """ Find roots of this to find z for T > T_C"""
    return g_nu1(z,3/2) - 2.612*T**(-3/2)

#%%
# Calculate ====================================================================
zplot = np.linspace(0.01,1.0,2**8) # range of z to plot BE functions over.
TovTc = np.linspace(0.01,3,2**8) # range of T/Tc to plot over

#calculate g functions for figure 1.
g32 = g_nu(zplot,3/2)
g52 = g_nu(zplot,5/2)

#calculate thermodynamic variables for figure 2.
N0ovN = 1 - (TovTc)**(3/2) # ground state population fraction for T< T_C
cV = 1.925*(TovTc)**(3/2) # specific heat for T < T_C
zfound = np.ones( (len(TovTc), )) # activity for T < T_C
I = np.argwhere(TovTc > 1) # find indices for temperatures above the transition temperature.  
N0ovN[I] = 0 # set N0 above transition temperature to zero.
NeovN = 1- N0ovN #Calculate total population in excited states.

for i in range(len(I)):
    n = I[i] #get ith index with T > T_C
    zfound[n] = brentq(ZeroMe,0.01,1,args = TovTc[n],xtol = 1E-8) # find z vs. T using constrained root finding.
    cV[n] = (15/4)*(g_nu1(zfound[n],5/2)/g_nu1(zfound[n],3/2)) - (9/4)*(g_nu1(zfound[n],3/2)/g_nu1(zfound[n],1/2)) # calculate specific heat for T > T_C

#%%
# Plot results ================================================================

fig = plt.figure()
ax = plt.axes()
ax.plot(zplot,g32, label= '$g_{3/2}(z)$')
ax.plot(zplot,g52, label = '$g_{5/2}(z)$')
ax.grid()
ax.legend()
ax.set_ylabel('$g_{\\nu}(z)$')
ax.set_xlabel('z')


fig, (ax1,ax2, ax3) = plt.subplots(3,1,sharex = True)
ax1.plot(TovTc,N0ovN, label = '$N_0/N$')
ax1.plot(TovTc,NeovN, label = "$N'/N$")
ax1.set_xlabel('$T/T_C$')
ax1.set_ylabel('Populations')
ax1.legend()
ax1.grid()

ax2.plot(TovTc,zfound)
ax2.set_xlabel('$T/T_C$')
ax2.set_ylabel('$z = e^{\\beta \mu}$')
ax2.grid()

ax3.plot(TovTc,cV)
ax3.set_xlabel('$T/T_C$')
ax3.set_ylabel('$c_V/N k_B$')
ax3.grid()