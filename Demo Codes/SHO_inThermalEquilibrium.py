"""
SHO_inThermalEquilibrium.py

This simple script calculates various things for a simple harmonic oscillator at temperature T

Origin: CHE 528 Lectures
Author: Tom Allison 2/17/2022
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rcParams.update({'font.size': 13})
matplotlib.rcParams.update({'figure.figsize': (8,10)})

hw = 518 # bend frequency of sulfur dioxide in wave numbers

def Q(T):
    kT = 208.5*(T/300) # convert temperature to wave number
    return 1/(1-np.exp(-hw/kT)) # Partition function for SHO neglecting zero-point energy

def p(v,T):
    kT = 208.5 * (T / 300)
    return np.exp(-v * hw/kT)/Q(T)

def E(T):
    """ Average thermal energy in units of kT"""
    kT = 208.5 * (T / 300)
    return (hw*np.exp(-hw/kT)/(1-np.exp(-hw/kT)))/kT

v = np.arange(20)


fig,(ax1,ax2,ax3,ax4, ax5) = plt.subplots(5, sharex = False)
ax1.bar(v,p(v,100), label = '$T$ = 100 K')
ax1.set_ylabel('$p_v$')
ax1.set_xticks(v)
ax1.set_xlabel('v')
ax1.legend()
ax1.set_title(f'SHO with $\hbar \omega$ = {hw} cm$^{{-1}}$')

ax2.bar(v,p(v,300), label = '$T$ = 300 K', color = 'black')
ax2.set_ylabel('$p_v$')
ax2.set_xticks(v)
ax2.set_xlabel('v')
ax2.legend()

ax3.bar(v,p(v,1000), label = '$T$ = 2000 K', color = 'red')
ax3.set_ylabel('$p_v$')
ax3.set_xticks(v)
ax3.set_xlabel('v')
ax3.legend()


ax4.bar(v,p(v,3000), label = '$T$ = 3000 K', color = 'magenta')
ax4.set_ylabel('$p_v$')
ax4.set_xticks(v)
ax4.set_xlabel('v')
ax4.legend()

T = np.linspace(10,10000,1000)
ax5.plot(T,E(T))
ax5.set_xlabel('$T$ [K]')
ax5.set_ylabel('<E>$(T)/k_B T$')
ax5.grid()

fig.tight_layout()

plt.show()