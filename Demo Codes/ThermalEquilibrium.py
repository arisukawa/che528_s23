"""
ThermalEquilibrium.py

This script looks at the number of microstates of a combined system as a function of energy sharing.
Makes plots that look like Commins Stat Mech notes fig. 1.1

Origin: CHE 528 Lecture Development
Author: Tom Allison
"""

#%%
# Preliminaries ===============================================================
import numpy as np
import matplotlib.pyplot as plt

#%%
# Declare variables ===============================================================
Mtot = 90 # total number oM degrees of freedom, shared between the two subsystems.
alpha = 1 # exponent describing integrated DOS of one degree of freedom.
epsbar = 1 # average energy per DOF.
C = 100   # Constant for single DOF integrated DOS.

#%%
# Declare functions ===============================================================
def Omega(E,M):
    """Returns number of states per unit energy, per Commins equation 1.8
       Can run into numerical problems for large M""" 
    eps1 = E/M              # energy per DOF
    Phi1 = C*eps1**alpha    # Number of states with energy < E.
    return (alpha/eps1)*(Phi1)**M # Omega

def S(E,M):
    """Returns (dimensionless) entropy corresponding to Omega(E,f)
       Better behaved for large M"""
    eps1 = E / M            # energy per DOF
    Phi1 = C*eps1**alpha    # Number of states with energy < E.
    return M*np.log(Phi1) + np.log(alpha/eps1) # Entropy

#%%
# Define energy array and distribute energy and DOFs ===============================
Etot = Mtot*epsbar 
E1 = np.linspace(0.1*Etot,0.9*Etot,2**10) # Scan the energy of system 1
E2 = Etot-E1                              # Keep the total energy constant.
M1 = Mtot/2 # you can change how the two systems share the degrees of freedom here.
M2 = Mtot-M1

#%%
# Caclulate number of states or entropy and make plots vs. E1 ===============================
if Mtot <= 100:
    """ For small systems with M < 100, it is reasonable to actually calculate Omega and plot"""
    fig, (ax1,ax2) = plt.subplots(2, sharex = True)
    ax1.semilogy(E1/Etot,Omega(E1,M1)) 
    ax1.semilogy(E1/Etot,Omega(E2,M2))
    ax1.semilogy(E1/Etot,Omega(E1,M1)*Omega(E2,M2),color = 'black')
    ax1.grid()
    ax1.set_xlabel('$E_1/E_{tot}$')
    ax1.set_title(f'$M$ = {Mtot}, $\\alpha$ = {alpha}')

    axR = ax2.twinx()
    axR.plot(E1/Etot,Omega(E1,M1), label = '$\Omega_1(E_1)$')
    axR.plot(E1/Etot,Omega(E2,M2), label = '$\Omega_2(E_{tot}-E_1)$')
    ax2.plot(E1/Etot,Omega(E1,M1)*Omega(E2,M2), color = 'black', label = '$\Omega_1 \Omega_2$')
    fig.legend()
    ax2.set_xlabel('$E_1/E_{tot}$')
    ax2.grid()
elif Mtot > 100:
    """For large system, must work with the logarithm S... not enough bits in computer numbers"""
    fig = plt.figure()
    ax = plt.axes()
    Stot = S(E1,M1) + S(E2,M2)
    Stot = Stot-np.max(Stot) #Subtract large offset so exponential is well behaved in the computer
    dPdE1 = np.exp(Stot) # un-normalized probability distribution proportional to number of microstates
    dPdE1 = dPdE1/np.trapz(dPdE1,E1/Etot) # normalize the probability distribution
    ax.plot(E1/Etot,dPdE1,label = '$P(E_1/E_{tot})$')
    ax.set_xlabel('$E_1/E_{tot}$')
    ax.set_ylabel('$P(E_1/E_{tot})$')
    ax.set_title(f'$f$ = {Mtot}, $\\alpha$ = {alpha}')
    ax.grid()

#%%
# Caclulate entropy and make plots vs. E1 =====================================================
fig, (ax1,ax2) = plt.subplots(2, sharex = True)
ax1.plot(E1/Etot,S(E1,M1), label = '$S_1(E_1)/k_B$')
ax1.plot(E1/Etot,S(E2,M2), label = '$S_2(E_{tot}-E_1)/k_B$')
ax1.grid()
ax1.legend()
ax1.set_xlabel('$E_1/E_{tot}$')
ax1.set_title(f'$M$ = {Mtot}, $\\alpha$ = {alpha}')
ax2.plot(E1/Etot,S(E1,M1) + S(E2,M2), color = 'black', label = '$(S_1 + S_2)/k_B$')
ax2.legend()
ax2.set_xlabel('$E_1/E_{tot}$')
ax2.grid()

#%% 
# Make plot showing behavior of probability distribution vs. M ================

fig = plt.figure()
ax = plt.axes()
ax.set_xlabel('$E_1/E_{tot}$')
ax.set_ylabel('$P(E_1/E_{tot})$')

Mplot = [10,Mtot,1000,10000]
for Mi in Mplot:
    M1 = Mi/2 # you can change how the two systems share the degrees of freedom here.
    M2 = Mi-M1
    Stot = S(E1,M1) + S(E2,M2)
    Stot = Stot-np.max(Stot) #Subtract large offset so exponential is well behaved in the computer
    dPdE1 = np.exp(Stot) # un-normalized probability distribution proportional to number of microstates
    dPdE1 = dPdE1/np.trapz(dPdE1,E1/Etot) # normalize the probability distribution
    ax.plot(E1/Etot,dPdE1,label = f'M = {Mi}')

ax.grid()
ax.legend()    
plt.show()

