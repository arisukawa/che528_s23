"""
SahaEqn.py

This script calculates equilibrium for the reaction
    H <--> e- + H+ 
using the Saha equation

Origin: CHE 528 Demos
Author: Tom Allison
"""

#%%
# Preliminaries ===============================================================
import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 14})

#%%
# Define parameters and calculate =============================================

hbar2ov2me = 3.810E-16 # \hbar^2/2m_e in units of eV-cm^2
kB = 8.62E-5 # Boltzmann's constant in units of eV/K
T = np.logspace(3,5,2**9)
Ip = 13.6 # ionization energy of Hydrogen atom
rho0 = [1E4, 3E10, 1E19] # initital density in 1/cm^3
lam_th_e = np.sqrt(4*np.pi*hbar2ov2me/(kB*T)) # thermal de Broglie length of the electron
gHplus = 2 # spin degeneracy of proton
gH = 4 # spin degeneracy of hydrogen atom ground state treating hyperfine levels as degenerate
X = []
labelstrings = []

for rho0i in rho0: 
    K = (2/lam_th_e**3)*(gHplus/gH)*np.exp(-Ip/(kB*T))
    S = K/rho0i
    I = np.argwhere(S > 1E4) 
    S[I] = 1E4 # cap S at 1E4 to avoid numerical issues.
    X.append(-S/2 + np.sqrt(S**2 + 4*S)/2) # solution to quadratic equation
    labelstrings.append(f'$\\rho = $ {rho0i:.1e} cm$^{{-3}}$')

X = np.transpose(X) # transpose for easy plotting.

#%%
# Define functions for top axis ===============================================
def kT(T):
    return kB*T

def TfromkT(kBT):
    return kBT/kB

# Make figure =================================================================

fig = plt.figure()
ax = plt.axes()
ax.semilogx(T,X, label = labelstrings)
ax.grid(True, which="both")
secax = ax.secondary_xaxis('top', functions=(kT, TfromkT))
secax.set_xlabel('$k_B T$ [eV]')
ax.set_xlabel('$T$ [K]')
ax.set_ylabel('Ionization fraction X')
ax.legend()

plt.show()
