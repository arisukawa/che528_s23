"""
ClassicalMonteCarlo.py

This script estimates thermodynamic parameters of a 1D (x,p) classical system.
It starts out set up for a simpler harmonic oscillator, but should for any 1D Hamiltonian.

Origin: CHE 528/PHY 540 Demo Codes
Author: Tom Allison
"""

#%% 
# Preliminaries ===============================================================


import numpy as np
import matplotlib.pyplot as plt
from numpy.random import rand
from numpy.random import randn
from scipy import integrate

def H(x,p):
    """ Hamiltonian - initially set to SHO but can be changed"""
    return x**2 + p**2 


#%%
# Run Monte Carlo Simulations =================================================

kT = 1

N = 30000
x = np.zeros( (N,) )
p = np.zeros( (N,) )
E = np.zeros( (N,) )
dE = np.zeros( (N,) )
w = np.zeros( (N,) )


for i in range(N):
    # print('----------------')   
    # print(f'iteration {i}')     
    if i == 0:
        x[0]=np.sqrt(kT)*(rand()-0.5)
        p[0]=np.sqrt(kT)*(rand()-0.5)
        E[0]= H(x[0],p[0])
    else:        
        dx = 1*np.sqrt(kT)*(rand()-0.5)
        dp = 1*np.sqrt(kT)*(rand()-0.5)
        Etrial = H(x[i-1] + dx,p[i-1] + dp)
        dE[i] = Etrial - E[i-1]
        if dE[i] < 0:
            E[i] = Etrial
            x[i] = x[i-1] + dx
            p[i] = p[i-1] + dp
            w[i] = 1
        else:
            X = rand()
            if np.exp(-dE[i]/kT) > X:
                # print('successful up attempt')
                E[i] = Etrial
                x[i] = x[i-1] + dx
                p[i] = p[i-1] + dp
                w[i] = 1
            else:
                # print('failed up attempt')
                E[i] = E[i-1] 
                x[i] = x[i-1]
                p[i] = p[i-1]
                w[i] = 0

# estimate observables from sampling
Istart = np.floor(N/5).astype('int') # take only last 4/5 of points, after initial anealing.
EbarMC = np.mean(E[Istart:])
xbarMC = np.mean(x[Istart:])
pbarMC = np.mean(p[Istart:])
EvarMC = np.var(E[Istart:])
xvarMC = np.var(x[Istart:])
yvarMC = np.var(x[Istart:])

# figure out transition rates used in MC simulatino
dEhist, binEdges = np.histogram(dE, bins = 100)
I = np.digitize(dE,binEdges)  # find bin numbers of each dE
binCenters = (binEdges[0:-1] + binEdges[1:])/2 # make bin centers.
wmean = []
for i in range(len(binCenters)):
    wmean.append(np.mean(w[np.argwhere(I == i)])) # calculate mean transition rate (average of 1's or 0's for each energy bin)

#%%    
# calculate partition functions via numerical integration =====================
# and calculate expectation values and fluctuations from Q ====================
beta0 = 1/kT
beta1 = (1+0.01)*1/kT
beta2 = beta1 + (beta1-beta0)

f0 = lambda pi,xi: np.exp(-beta0*H(xi,pi))
f1 = lambda pi,xi: np.exp(-beta1*H(xi,pi))
f2 = lambda pi,xi: np.exp(-beta2*H(xi,pi))

xmin = np.min(x) + (np.min(x)-np.mean(x))
xmax = np.max(x) + (np.max(x)-np.mean(x))
pmin = np.min(p) + (np.min(x)-np.mean(p))
pmax = np.max(p) + (np.max(x)-np.mean(p))

Q0,err = integrate.dblquad(f0, xmin, xmax, gfun = lambda x: pmin, hfun = lambda x: pmax) #2D integration. 
Q1,err = integrate.dblquad(f1, xmin, xmax, gfun = lambda x: pmin, hfun = lambda x: pmax)
Q2,err = integrate.dblquad(f2, xmin, xmax, gfun = lambda x: pmin, hfun = lambda x: pmax)

Ebar = -(np.log(Q1) - np.log(Q0))/(beta1 - beta0) # Energy via derivative of partition function   
Evar = (np.log(Q2) - 2*np.log(Q1) + np.log(Q0))/(beta1 - beta0)**2 # second derivative for variance

#print(v)

#%%
# Plot results ================================================================

fig = plt.figure()
ax = plt.axes()
ax.plot(E, zorder = 1)
ax.hlines(Ebar,0,N, linestyle = '-', color = 'black', label = 'Direct Calculation', linewidth = 3, zorder = 2)
ax.hlines(np.mean(E[np.floor(N/5).astype('int'):]),0,N,linestyle = '--', color = 'red', label = 'Monte Carlo Average', linewidth = 3, zorder = 3)
ax.set_xlabel('iteration')
ax.set_ylabel('Energy [a.u.]')
ax.legend()

fig, axax = plt.subplots(2,2,sharex = False)
axax[0,0].hist2d(x,p, bins = [100,100]) # bins = [100,100])
axax[0,0].set_aspect('equal')
axax[0,0].set_xlabel('x [a.u.]')
axax[0,0].set_ylabel('p [a.u.]')

axax[1,0].hist(x,bins = 100)
axax[1,0].set_xlabel('x [a.u.]')
axax[1,0].set_ylabel('Occurences')

axax[0,1].hist(p,bins = 100, orientation = 'horizontal')
axax[0,1].set_ylabel('p [a.u.]')
axax[0,1].set_xlabel('Occurences')

fig = plt.figure()
ax = plt.axes()
plt.plot(binCenters,wmean, label = 'Monte Carlo Transition Rate', marker = 'o')
dEshow = np.linspace(0,np.max(dE))
plt.plot(dEshow,np.exp(-dEshow/kT),linestyle = '--', color = 'black', label = 'exp($-\\beta \Delta E)$')
ax.set_xlabel('$\Delta E$ [a.u.]')
ax.set_ylabel('Transtion rate w [a.u.]')
ax.legend()

plt.show()

                          
