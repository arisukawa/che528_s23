"""
OneDAtomChainHeatCapacity_demo.py

This script calculates the thermal energy and heat capacity of a 
1D atom chain vs. temperature. 

Origin: CHE 528 lectures
Author: Tom Allison
"""

#%% 
# Preliminaries ===============================================================
import numpy as np
import matplotlib.pyplot as plt

#%%
# Set up dispersion relation calculate =======================================
ka = np.linspace(-np.pi,np.pi,2**16)
dka = ka[1]-ka[0]
w0 = 1 # \sqrt(2k_1/m) natural frequency if only one atom moving.
w = 2*w0*np.abs(np.sin(ka/2)) # dispersion relation


#%%
# Calculate energy and heat capacity vs. temperature ==========================

Tp = np.linspace(0.001,2,300) # temperature in units of hbar*w0/kB

#initialize variables
E = np.zeros( (len(Tp),) )
cL = np.zeros( (len(Tp),) )

for j in range(len(Tp)):
    E[j] = (1/(2*np.pi))*dka*np.trapz( w/(np.exp(w/Tp[j])-1) )
    if j > 0:
        cL[j] = (E[j]-E[j-1])/(Tp[j]-Tp[j-1]) # calculate heat capacity by finite difference
 
#%%
# Plot results ===============================================================  
        
fig = plt.figure()
ax = plt.axes(xlim = (-np.pi,np.pi), ylim = (0,2.2*w0))
ax.plot(ka,w)
ax.set_xlabel('$ka$ [rad]')
ax.set_ylabel('$\omega(k)/\omega_0$')
ax.grid()

fig, (ax1,ax2) = plt.subplots(2,1,sharex = True)
ax1.plot(Tp,E)
ax1.grid()
ax2.set_xlabel('$k_B T / (\hbar \omega_0)$')
ax1.set_ylabel('$<E> \;\; [(L/a) \hbar \omega_0]$')

ax2.plot(Tp,cL)
ax2.grid()
ax2.set_xlabel('$k_B T / (\hbar \omega_0)$')
ax2.set_ylabel('$c_L \;\; [k_B/a]$')


