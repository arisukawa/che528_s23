"""
FreeEnergyPlots.py

This script looks at the free energy of an ensemble of N (distinguishable) 
two-level atoms as the temperature is varied.

Origin: PHY540 Lecture Development
Author: Tom Allison
"""

#%% 
# Preliminaries ===============================================================

import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 12})
from scipy.special import gamma

#%%
# Define Global Variables =====================================================
N = 100 #Number of atoms
eps = 1
kB = 1

#%%
# Define functions ============================================================

def S(E):
    """ Entropy in units of kB from combinatorics. Use gamma function so works for non-integer excitation"""
    m = E/eps
    return kB*np.log(gamma(N+1)/(gamma(N+1-m)*gamma(m+1)))

#%%
# Set up problem ==============================================================

E = np.linspace(0,N*eps) #Total Energy as a continuous variable

kT = np.asarray([0.1, 1, 3, 10])*eps #Selected temperatures to plot A(T)
kTsmooth = np.logspace(-2,np.log10(10*np.max(kT)),200) # log-scale temps to plot probabilities
q1 = 1 + np.exp(-eps/kTsmooth) # single-atom partition function
p1_0 = np.exp(-0/kTsmooth)/q1 # probability for excited state
p1_1 = np.exp(-eps/kTsmooth)/q1 # probability for ground state

#%%
# Plot state probabilities and total entropy ==================================

fig, (ax1, ax2) = plt.subplots(2,1) 
ax1.semilogx(kTsmooth,p1_0,label = '$p_0(T)$')
ax1.semilogx(kTsmooth,p1_1,label = '$p_1(T)$')
ax1.set_xlabel('$kT/\\varepsilon$')
ax1.set_ylabel('State Probabilities')
ax1.legend()
ax1.grid()

ax2.plot(E,S(E), color = 'black')
ax2.set_xlabel('$\\langle E \\rangle / \\varepsilon$')
ax2.set_ylabel('$S/k_B$')
ax2.grid()

#%%
# Calculate and plot free energy vs. E for different T ========================

Ebar = [] #initialize empty list for <E>
Amin = [] #initialize empty list for minimum A
Ai = {}   # iniitialize empty dictionary for storing arrays for each T.

fig, (ax1, ax2) = plt.subplots(2,1,sharex = False) 

for kTi in kT: 
    Ebar.append( N*eps*( np.exp(eps/kTi) +1 )**(-1) )
    Ai.update({kTi:E-(kTi/kB)*S(E)}) # calculate array of free energy vs. E for a certain temperature.
    ax2.plot(E,Ai[kTi], label = f'$kT$ = {kTi}$\\varepsilon$')
    Amin.append(np.min(Ai[kTi]))

ax2.plot(Ebar,Amin, marker = 'o', color = 'black', label = '$\\langle E \\rangle (T) /\\varepsilon$')
ax2.set_xlabel('$\\langle E \\rangle / \\varepsilon$')
ax2.set_ylabel('$A = \\langle E \\rangle-TS \; [\\varepsilon]$')
ax2.grid()
ax2.legend()

ax1.plot(kT,Ebar, marker = 'o', label = '$\\langle E \\rangle /\\varepsilon$')
ax1.set_xlabel('$kT/\\varepsilon$')
ax1.set_ylabel('$ \\langle E \\rangle /\\varepsilon $')
ax1.grid()
#ax1.legend()

plt.show()

