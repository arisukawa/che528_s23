# CHE528_S23

Welcome to CHE 528: Statistical Mechanics at Stony Brook University

## Repository Info

This Gitlab repository will serve as the main course webpage for the fall 2022 edition of Physics 540 at Stony Brook University. For the official description of the course please look at the syllabus.
You can clone the repository to get the latest course documents and codes. Pull periodically to get the latest udpates and bug fixes.

## Problem Solutions

Solutions to the homework problems can be found here: https://drive.google.com/file/d/1eMr2XHnZbv0SuEKHCw9Rn2NtfKgOryEe/view?usp=sharing
